import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import MovieList from "./components/movie_list/index";
import Movie from "./components/movie/index";
import Navbar from "./assets/navbar/index";

const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<MovieList />} />
        <Route path="/movie/:id" element={<Movie />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
