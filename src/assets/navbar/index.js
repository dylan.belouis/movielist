import { Link } from "react-router-dom";
import movieLogo from "../image/movie_logo.png";
import "./index.css";

const Navbar = () => {
  return (
    <div className="Navbar">
      <div className="Navbar-logo_display">
        <Link className="Navbar-logo" to="/">
          <div className="Navbar-logo">
            <img
              className="movie-logo_Navbar"
              src={movieLogo}
              alt="movie-logo"
            />
          </div>
        </Link>
      </div>
    </div>
  );
};

export default Navbar;
