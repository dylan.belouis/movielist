import { useState, useEffect } from "react";
import axios from "axios";
import "./index.css";

const MovieSearch = () => {
  const [useSearchFilter, setSearchFilter] = useState("");
  const [useSearchData, setSearchData] = useState([]);

  const getSearchMovieData = () => {
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=${process.env.REACT_APP_API_KEY}&query=${useSearchFilter}&language=fr`
      )
      .then((response) => {
        const data = [response.data];
        setSearchData(data[0]);
      });
  };

  useEffect(() => {
    // eslint-disable-next-line
  }, [useSearchFilter]);

  return (
    <div className="MovieSearch">
      <p>Search :</p>
      <input
        type="text"
        placeholder="recherche"
        value={useSearchFilter}
        onChange={(e) => setSearchFilter(e.target.value)}
      />
      <button onClick={getSearchMovieData}>Chercher</button>
    </div>
  );
};

export default MovieSearch;
