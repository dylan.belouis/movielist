import { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "./index.css";

const PreviewMoviesBar = ({ sectionTitle }) => {
  const [useData, setData] = useState([]);

  const getGenreMovieData = () => {
    axios
      .get(
        `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.REACT_APP_API_KEY}&language=fr-FR`
      )
      .then((response) => {
        const data = [response.data];
        setData(data[0]);
      });
  };

  useEffect(() => {
    getGenreMovieData();
    // eslint-disable-next-line
  }, [useData]);

  return (
    <div className="preview_movies_bar">
      <p className="title_section_MovieList">{sectionTitle}</p>
      <div className="block_templates_MovieList">
        {useData.results &&
          useData.results.map((data, index) => {
            return (
              <Link to={`/movie/${data.id}`}>
                <div
                  id={index}
                  className="template_card_MovieList"
                  key={`${index}-mostrecommanded`}
                >
                  <img
                    className="img_card_MovieList"
                    src={`https://image.tmdb.org/t/p/original${data.poster_path}`}
                    alt="movie jacket"
                  />
                </div>
              </Link>
            );
          })}
      </div>
      <a href="#13">Suite</a>
    </div>
  );
};

export default PreviewMoviesBar;
