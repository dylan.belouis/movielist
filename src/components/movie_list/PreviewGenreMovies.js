import { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "./index.css";

const PreviewGenreMovies = ({ genre, sectionTitle }) => {
  const [useGenreMovieData, setGenreMovieData] = useState([]);

  const getGenreMovieData = () => {
    axios
      .get(
        `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.REACT_APP_API_KEY}&language=fr&with_genres=${genre}`
      )
      .then((response) => {
        const data = [response.data];
        setGenreMovieData(data[0]);
      });
  };

  useEffect(() => {
    getGenreMovieData();
    // eslint-disable-next-line
  }, [useGenreMovieData, genre]);

  return (
    <div className="preview_movies_bar">
      <p className="title_section_MovieList">{sectionTitle}</p>
      <div className="block_templates_MovieList">
        {useGenreMovieData.results &&
          useGenreMovieData.results.map((data, index) => {
            return (
              <Link to={`/movie/${data.id}`}>
                <div
                  className="template_card_MovieList"
                  key={`${index}-${genre}`}
                >
                  <img
                    className="img_card_MovieList"
                    src={`https://image.tmdb.org/t/p/original${data.poster_path}`}
                    alt="movie jacket"
                  />
                </div>
              </Link>
            );
          })}
      </div>
    </div>
  );
};

export default PreviewGenreMovies;
