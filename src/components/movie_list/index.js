import PreviewMoviesBar from "./PreviewMostRecommandedMovies";
import PreviewGenreMovies from "./PreviewGenreMovies";
import "./index.css";

const MovieList = () => {
  return (
    <div className="MovieList">
      <PreviewMoviesBar sectionTitle="Les plus recommandés" />
      <PreviewGenreMovies genre="28" sectionTitle="Action" />
      <PreviewGenreMovies genre="35" sectionTitle="Comédie" />
      <PreviewGenreMovies genre="12" sectionTitle="Aventure" />
      <PreviewGenreMovies genre="80" sectionTitle="Crime" />
    </div>
  );
};

export default MovieList;
