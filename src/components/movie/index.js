import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import "./index.css";
import RevenueLogo from "../../assets/image/revenue.png";
import BudgetLogo from "../../assets/image/portefeuille.png";

const Movie = () => {
  const [useApiData, setApiData] = useState([]);
  const [useCompanies, setCompanies] = useState([]);
  const [useGenres, setGenres] = useState([]);
  const [useUrlYoutubeTeaser, setUrlYoutubeTeaser] = useState("");
  let { id } = useParams();
  console.log(id);

  const getMovieData = () => {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.REACT_APP_API_KEY}&language=fr-FR`
      )
      .then((res) => {
        setApiData(res.data);
        setGenres(res.data.genres);
        setCompanies(res.data.production_companies);
        setYoutubeTeaser(res.data.title);
      });
  };

  async function setYoutubeTeaser(url) {
    const lowerUrl = url.toLowerCase();
    const regex = lowerUrl.replace(/ /g, "+");
    console.log(regex);
    const searchYoutube =
      await `https://youtube.googleapis.com/youtube/v3/search?q=${regex}%20bande%20annonce%20vf%20&key=${process.env.REACT_APP_YOUTUBE_API_KEY}`;
    axios.get(searchYoutube).then((res) => {
      setUrlYoutubeTeaser(res.data.items[0].id.videoId);
      console.log(res);
    });
    setUrlYoutubeTeaser(url);
  }

  useEffect(() => {
    getMovieData();
    // eslint-disable-next-line
  }, [id]);
  return (
    <div className="Movie">
      <div className="presentation-container">
        <img
          className="movie-img"
          src={`https://image.tmdb.org/t/p/original${useApiData.backdrop_path}`}
          alt="poster"
        />
        <div className="movie-presentation">
          <p>{useApiData.title}</p>
          <p className="movie-tagline">{useApiData.tagline}</p>
        </div>
      </div>
      <p className="text-description-movie">
        {`Récommandé à ${useApiData.vote_average * 10}% avec ${
          useApiData.vote_count
        } votes`}
      </p>
      <div className="movie-description">
        <div className="genres_movie_description">
          {useGenres.map((genre, index) => (
            <div className="item_genre_movie" key={index}>
              <p>{genre.name}</p>
            </div>
          ))}
        </div>
        <div className="div_youtube_movie_video">
          <iframe
            className="youtube_movie_video"
            src={`https://www.youtube.com/embed/${useUrlYoutubeTeaser}`}
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen="allowfullscreen"
          ></iframe>
        </div>
        <p className="text-description-movie">{useApiData.overview}</p>
        {useApiData.budget > 0 && (
          <div className="movie_infos">
            <img
              className="budget_revenue_logo"
              src={BudgetLogo}
              alt="budget"
            />
            <p className="title_budget_revenue">Budget</p>
            <p className="text_revenue_movie">{`${useApiData.budget} $`}</p>
          </div>
        )}
        {useApiData.revenue > 0 && (
          <div className="movie_infos">
            <img
              className="budget_revenue_logo"
              src={RevenueLogo}
              alt="revenue"
            />
            <p className="title_budget_revenue">Revenue</p>
            <p className="text_revenue_movie">{`${useApiData.revenue} $`}</p>
          </div>
        )}
        <p className="category_name_movie">Compagnie de production :</p>
        <div className="box_company">
          {useCompanies.map((data, index) => (
            <div key={index} className="box_item_company">
              {data.logo_path && (
                <img
                  className="logo_company"
                  src={`https://image.tmdb.org/t/p/original${data.logo_path}`}
                  alt="logo"
                />
              )}
              <p>{data.name}</p>
            </div>
          ))}
        </div>
      </div>
      <button onClick={() => console.log(useApiData)}>view data</button>
    </div>
  );
};

export default Movie;
